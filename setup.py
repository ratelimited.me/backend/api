from setuptools import setup, find_packages

setup(
    name="api",
    version="0.0.1",
    description="Image host",
    url="https://ratelimited.me",
    author="George Tsatsis",
    python_requires=">=3.7",
    packages=find_packages(),
    package_dir={"": "."},
)
