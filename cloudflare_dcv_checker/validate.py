import aiohttp
import config


async def txt(domain, token, min_servers=7):
    """Validate the control of a domain using a TXT record.

    Args:
        domain (str): the domain to validate control of (with subdomains, no protocol)
        token (str): the value of the txt record
        min_servers (int, optional): How many servers should return true for the check to pass. Defaults to 7.

    Returns:
        bool: whether the check passed or not
    """
    async with aiohttp.ClientSession() as session:
        async with session.post(
            "https://dcvcheck.cloudflare.com/check",
            json={
                "authToken": config.CLOUDFLARE_DCV_AUTHTOKEN,
                "method": "TXT",
                "verbose": "true",
                "params": {"domain": domain, "expectedResponse": token},
            },
        ) as resp:
            resp = await resp.json()
            if (
                resp["success_servers"] >= min_servers
            ):  # TODO: Correct the response value.
                return True

            return False


async def cname(domain, challenge, point_to, min_servers=7):
    """Validates the control of a domain using CNAME records

    Args:
        domain (str): the domain to validate control of (without subdomains, no protocol)
        challenge (str): the domain to validate control of (with subdomains, no protocol)
        point_to (str): where the cname should point to
        min_servers (int, optional): How many servers should return true for the check to pass. Defaults to 7.

    Returns:
        bool: whether the check passed or not
    """
    challenge += "." + domain

    async with aiohttp.ClientSession() as session:
        async with session.post(
            "https://dcvcheck.cloudflare.com/check",
            json={
                "authToken": config.CLOUDFLARE_DCV_AUTHTOKEN,
                "method": "CNAME",
                "verbose": "true",
                "params": {
                    "domain": domain,
                    "challenge": challenge,
                    "expectedResponse": point_to,
                },
            },
        ) as resp:
            resp = await resp.json()
            if (
                resp["success_servers"] >= min_servers
            ):  # TODO: Correct the response value.
                return True

            return False


async def http(domain, challenge, token, min_servers=7):
    """Validate the control of a domain using http (http-01)

    Args:
        domain (str): the domain to validate control of (with subdomains, no protocol)
        challenge (str): <domain>/.well-known/acme-challenge/<challenge>
        token (str): <challenge> + . + <token>
        min_servers (int, optional): How many servers should return true for the check to pass. Defaults to 7.

    Returns:
        bool: whether the check passed or not
    """
    async with aiohttp.ClientSession() as session:
        async with session.post(
            "https://dcvcheck.cloudflare.com/check",
            json={
                "authToken": config.CLOUDFLARE_DCV_AUTHTOKEN,
                "method": "HTTP",
                "verbose": "true",
                "params": {
                    "domain": domain,
                    "challenge": challenge,
                    "expectedResponse": token,
                },
            },
        ) as resp:
            resp = await resp.json()
            if (
                resp["success_servers"] >= min_servers
            ):  # TODO: Correct the response value.
                return True

            return False