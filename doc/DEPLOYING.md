# Deploying the RATELIMITED API (BM/QEMU/LXC Edition)

### Notice: This document was directly copied from our internal Wiki.JS instance. Some Wiki.JS-specific markdown (such as is-info, is-warning) may not render correctly. Additionally, referenced Wiki paths can be dismissed.

> This document assumes that you are deploying on a _non-orchestrating host_. Be that a _bare metal machine_, a _QEMU VM_, or an _LXC/LXD container_. For _Docker_, _Nomad_, _Terraform_, or other methods of deploying the API, please look at the other available guides in RATELIMITED/api/Deploying.
>
> Additionally, this document focuses on a production-grade deployment, and not a testing deployment. To run a testing deployment (canary/beta/development), please refer to the "Setting up a development environment" guide in the base RATELIMITED category.
> {.is-info}

> The commands that are found within this guide apply to Debian, and Debian-based Linux distributions. If you choose to install the API on something that is _not_ documented as working, please be ready to change the commands to fit your distribution.
> {.is-warning}

## Host Initialization

The first step of every deployment on bare metal is being up-to-date. Begin by installing the latest version of a Linux distribution of choice For the sake of simplicity, this guide is written with **Debian 10 ("Buster")** in mind.

After having initialized your host, and having logged on as a user that is _not_ root, but has `sudo` permissions<a href="#superscripts"><sup>1</sup></a>, please update it by running `sudo apt update && sudo apt upgrade -yqq`<a href="#subscripts-codecommand-snippets"><sub>[1]</sub></a> (`-yqq` will skip prompting you about if you would like to proceed with the installation, and will also silence a lot of _apt_'s activity). Once the host is completely up-to-date, reboot. A restart ensures that if there were any updates to the kernel, they will be applied.

Proceed by creating a directory in `/opt/`, usually, a good precautionary step is to actually clone the repository of the API, so you can update it with a single command later. Cloning the repository is as simple as running `git clone https://gitlab.com/ratelimited.me/backend/api.git /opt/api`<a href="#subscripts-codecommand-snippets"><sub>[3]</sub></a>.

Once that's done, you should create a user that will run the API service (**never run the service as a privileged user**). To do that, run `sudo adduser --system --no-create-home --group api`<a href="#subscripts-codecommand-snippets"><sub>[4]</sub></a>, which will create a _system_ user that does _not_ have a home directory, _and_ will also create the `api` group. This account will be locked later in the guide to prevent logging in by unauthorized parties.

From here, it is required that the api user can access the `/opt/api` directory created in the previous step. Change the owner of the directory to be user `api` and group `api` (`api:api`) by running `sudo chown -R api:api /opt/api`<a href="#subscripts-codecommand-snippets"><sub>[5]</sub></a>.

## Application Environment Initialization

Ensure that Python (preferably, 3.8 or above) is installed, by running `python -V`. If `python` is not found, try `python3` or `py` instead, before attempting to install the latest version of Python by running `sudo apt install python3 python3-pip -yqq`<a href="#subscripts-codecommand-snippets"><sub>[6]</sub></a>.

After ensuring Python is installed and available, as a privileged user, run `sudo -H pip install virtualenv`<a href="#subscripts-codecommand-snippets"><sub>[7]</sub></a>.

Now that the `api` user is able to access the `/opt/api` directory, and the required Python packages for initialization have been installed, change to the user by using `sudo -u api bash`, and go into the directory itself.

Having logged in as the `api` user, in the `/opt/api` directory, run `virtualenv venv`<a href="#subscript-8-creating-and-activating-the-python-virtualenv"><sub>[8]</sub></a>, which will initialize a Python virtual environment with the system-wide default Python interpreter. (`python`, usually), and activate the virtualenv by running `source venv/bin/activate`<a href="#subscripts-codecommand-snippets"><sub>[8]</sub></a>.

Proceed to install all the required Python packages for the API by running `pip install -r requirements.txt`<a href="#subscripts-codecommand-snippets"><sub>[9]</sub></a>.

## Configuring the API

Now that all the requirements have been installed, it's time to configure the API itself. Copy over the `config.example.py` to `config.py`, and edit it to your liking. More specifically, each option is documented below.

**If you do not need an explanation of each configuration value, please jump to [Setting up the Systemd Unit file](#setting-up-the-systemd-unit-file).**

### Configuration Description

#### Spawning

- `HOST`: This is the IP address that the API will listen on if called directly, instead of being spawned with an ASGI-compatible server (Hypercorn).
- `PORT`: This is the TCP port that the API will listen on if called directly, instead of being spawned in with an ASGI-compatible server (Hypercorn).

#### Variables/Strings

- `SERVICE_NAME` _(str)_: This is used in emails to identify the service.
- `BASE_URL` _(str)_: This is used to form the entire URL in emails. It should not have a trailing slash.
- `SUPPORT_EMAIL` _(str)_: This is used in the reply-to function of emails, as well as when something goes wrong (Error 5XX).

#### [Cloudflare Domain Control Validation](https://blog.cloudflare.com/secure-certificate-issuance/)

- `CLOUDFLARE_DCV_ENABLED` _(bool)_: This enables the use of the [Cloudflare Domain Control Validation](https://blog.cloudflare.com/secure-certificate-issuance/) API. If disabled, the `dns` Python library is used instead (single point of domain validation).
- `CLOUDFLARE_DCV_AUTHTOKEN` _(str)_: This is the authentication token provided to you by Cloudflare. (Only applicable if DCV is enabled).

#### Registration

- `REGISTRATIONS_ENABLED` _(bool)_: This controls whether self-registration is allowed on the instance. If disabled, an administrator must manually create the account.
- `REGISTRATIONS_REQUIRE_APPROVAL` _(bool)_: This controls whether self-registration requires administrative approval. The difference between this, and manually creating an account with self-registration disabled is that the user can provide their own details, instead of having to let an administrator set them.
- `REGISTRATIONS_REQUIRE_CAPTCHA` _(bool)_: This controls whether a self-registration will require that a CAPTCHA is filled in.

#### Database Information

_Note regarding different DBMS: The only DBMS currently supported is PostgreSQL, and there are no plans to expand support to other DBMS, unless required by further developments (Redis is a great example, as the rate-limiter, when used in Hypercorn with multiple workers, requires that Redis is used to synchronize the rate-limits among workers)._

**_For further information regarding setting up PostgreSQL (including setting up the database from the included template file), please read RATELIMITED/api/Deploying/DBMS/Postgres/Production._**

- `host` _(str)_: This is the IP address that the DBMS listens on.
- `port` _(str)_: This is the TCP port that the DBMS listens on.
- `database` _(str)_: This is the name of the database itself.
- `user` _(str)_: This is the username that will be used to authenticate against the DBMS. Ensure it has permissions to read from, and write to the proper database.
- `password` _(str)_: This is the password that will be used to authenticate against the DBMS.
- `min_size` _(int)_: This is the minimum size of the database connection pool. Each worker will have a minimum of _`min_size`_ connections open at any given time.
- `max_size` _(int)_: This is the maximum size of the database connection pool Each worker will have a maximum of _`max_size`_ connections open at any given time.
- `max_inactive_connection_lifetime` _(int)_: This is the lifetime of an allocated connection by a worker in seconds. If the connection is inactive for longer than _`max_inactive_connection_lifetime`_, it will be released back into the connection pool.

#### Tokens

- `TOKEN_SIGNING_KEY` _(str)_: This is the secret key used to sign tokens. Ensure that this is a strong, random value.
- `TIMED_TOKEN_AGE` _(int)_: This is the expiry time for a _timed_ token, in seconds.
- `VERIFICATION_TOKEN_AGE` _(int)_: This is the expiry time for a _verification_ token, in seconds.
- `UNVERIFIED_ACCOUNT_DELETION_TOKEN_AGE` _(int)_: This is the expiry time for a _unverified account deletion_ token, in seconds.

#### CAPTCHA

- `CAPTCHA_SITEVERIFY_URL` _(str)_: This is the siteverify URL of your CAPTCHA provider. Please note that internally, this will be called with a POST request.
- `CAPTCHA_SECRET` _(str)_: This is the secret key provided to you by your CAPTCHA provider.

#### S3 Information

- `endpoint` _(str)_: This is the IP + port combination of the Minio (or S3-compatible) Storage Service.
- `access_key` _(str)_: This is the S3 Access Key.
- `secret_key` _(str)_: This is the S3 Access Key Secret.
- `secure` _(bool)_: This configures if TLS should be used during communication with the S3 service.

- `S3_DEFAULT_BUCKET` _(str)_: This is the default bucket that should be used in the event that no bucket is specified in an upload request.

#### Uploading Limitations

- `DEFAULT_UPLOAD_LIMIT` _(int)_: This is the default upload limit, used in the case that the user does not have any upgrades. In **mebibytes (MiB)**.
- `MAXIMUM_SYSTEMWIDE_UPLOAD_LIMIT` _(int)_: This is the maximum size of a request that the webserver (Quart/Hypercorn, depending on the configuration) will handle. In **mebibytes (MiB)**.

#### E-Mail Information

- `EMAIL_PROVIDER` _(str)_: This can be either `sendgrid` or `smtp`. It is used to define which handler will be sending emails.
- `SEND_FROM_EMAIL` _(str)_: This is the email address that emails should be sent _from_.

- `SMTP_HOST` \*(str) **[optional if Sendgrid is used]\***: This is the host that should be used to send emails with SMTP.
- `SMTP_PORT` \*(int) **[optional if Sendgrid is used]\***: This is the port that should be used to send emails with SMTP.
- `SMTP_USERNAME` \*(str) **[optional if Sendgrid is used]\***: This is the username that should be used to authenticate to the SMTP server.
- `SMTP_PASSWORD` \*(str) **[optional if Sendgrid is used]\***: This is the password that should be used to authenticate to the SMTP server.

- `SENDGRID_API_KEY` \*(str) **[optional if SMTP is used]\***: This is your Sendgrid API key.

## Setting up the Systemd Unit file

Once the configuration is all set up and ready to go, you'll want to set up the systemd configuration file so the api can be managed as a service.

Go ahead and create a file in `/etc/systemd/system/ratelimited-api.service`, with the following contents.

```
[Unit]
Description=RATELIMITED API Service
After=network.target

[Service]
User=api
Restart=always
WorkingDirectory=/opt/api
ExecStart=/opt/api/venv/bin/python -m hypercorn app:app --bind 127.0.0.1 -w 16


[Install]
WantedBy=multi-user.target
```

After you're done creating the file, you should now be able to run `sudo systemctl daemon-reload && sudo systemctl enable --now ratelimited-api`<a href="#subscripts-codecommand-snippets"><sub>[10]</sub></a>, which should enable the service to run at startup, as well as make it run.

## Footnotes (Superscripts, Subscripts)

### Superscripts:

- 1: `sudo` does not come preinstalled with Debian 10. A recommended initialization install-kit after installation is `apt update && apt install sudo curl wget git gnupg2`<a href="#subscripts-codecommand-snippets"><sub>[2]</sub></a>.

### Subscripts (Code/Command Snippets):

- 1. Upgrading the host. _[non-root]_:
  ```
  sudo apt update && sudo apt upgrade -yqq
  ```
- 2. Installing `sudo`, `curl`, `wget`, `git`, and `gnupg2` _[as root]_:
  ```
  apt update && apt install sudo curl wget git gnupg2
  ```
- 3. Getting a fresh copy of the API's source code. _[non-root]_:
  ```
  git clone https://gitlab.com/ratelimited.me/backend/api.git /opt/api
  ```
- 4. Creating the system user that will be used to run the API. _[non-root]_:
  ```
  sudo adduser --system --no-create-home --group api
  ```
- 5. Granting permission to the user to access the API source code directory. _[non-root]_:
  ```
  sudo chown -R api:api /opt/api
  ```
- 6. Installing Python 3.x as well as the pip package manager. _[non-root]_:
  ```
  sudo apt install python3 python3-pip -yqq
  ```
- 7. Installing the `virtualenv` Python package. _[non-root]_:
  ```
  sudo -H pip install virtualenv
  ```
- 8. Creating and activating the Python virtualenv:
  ```
  virtualenv venv && source venv/bin/activate
  ```
- 9. Installing all Python packages required for the API:
  ```
  pip install -r requirements.txt
  ```
- 10. Enabling and starting the Systemd Unit:
  ```
  sudo systemctl daemon-reload \
  && sudo systemctl enable --now ratelimited-api
  ```
