from quart import current_app as app
from .errors import APIError


async def get_user(user_id=None, username=None, email=None, context=""):
    """Fetches a user by the given identifier.
    Args:
        user_id (str): The UUID of a given user
        username (str): The username of a given user
        email (str): The email of a given user
        context: What kind of context should we deliver? (Valid: [auth, empty string])
    Returns:
        dict: The user information according to the given context
    """

    # Ensure that only one constraint is being used to get a user
    assert user_id or username or email

    assert not (user_id and email) and not (user_id and username)
    assert not (email and username)

    context = context.lower()
    search_field = "id" if user_id else "username" if username else "email"

    if context == "auth":
        res = await app.db.fetchrow(
            f"SELECT id, username, email, password, verified, banned, admin, mfa, mfa_secret FROM users WHERE {search_field} = $1",
            user_id or username or email,
        )
    else:
        res = await app.db.fetchrow(
            f"SELECT id, username, email, verified, banned, admin, mfa FROM users WHERE {search_field} = $1",
            user_id or username or email,
        )

    if not res:
        return None

    user = {
        "id": res["id"],
        "username": res["username"],
        "email": res["email"],
        "verified": res["verified"],
        "banned": res["banned"],
        "admin": res["admin"],
        "mfa_enabled": res["mfa"],
    }

    if context == "auth":
        user["password"] = res["password"]
        user["mfa_secret"] = res["mfa_secret"]

    return user


async def get_users(constraint="all"):
    """Gets all users based on the given constraint.
    Args:
        constraint (str): The constraint (oneof: active, inactive, banned, *: (none))
    Returns:
        list: All the registered users and their metadata according to non-contextual get_user
    """
    users_list = []
    constraint = constraint.lower()

    if constraint == "active":
        users = await app.db.fetch("SELECT id FROM users WHERE verified = true")
    elif constraint == "inactive":
        users = await app.db.fetch("SELECT id FROM users WHERE verified = false")
    elif constraint == "banned":
        users = await app.db.fetch("SELECT id FROM users WHERE banned = true")
    else:
        users = await app.db.fetch("SELECT id FROM users")

    for db_user in users:
        user = await get_user(user_id=db_user["id"])

        users_list.append(user)

    return users_list


async def get_user_id_by_username(username):
    """Gets the ID of a user based on the user's username.
    Args:
        username: The user's username.
    Returns:
        string: The ID of the user from the database.
    """
    return await app.db.fetchval("SELECT id FROM users WHERE username = $1", username)


async def get_user_id_by_email(email):
    """Gets the ID of a user based on the user's email.
    Args:
        email: The user's username.
    Returns:
        string: The ID of the user from the database.
    """
    return await app.db.fetchval("SELECT id FROM users WHERE email = $1", email)
