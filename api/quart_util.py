from quart import request


async def rate_limiter_get_real_ip():
    # Return the X-Forwarded-For as the user-agent identifier,
    # unless it isn't present (direct connection).
    return request.headers.get("X-Forwarded-For", request.remote_addr)
