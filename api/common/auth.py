import re

from .. import storage
from ..common import identifiers
from ..errors import BadRequest, FailedLogin
from ..schema import Validate, TOKEN_CHECK_SCHEMA
from ..common.email import send_mail

from quart import jsonify, request
from quart import current_app as app

from itsdangerous import TimestampSigner

import bcrypt
import pyotp


async def user_exists(email, username):
    """Checks if a user with the given email or username exists."""
    if (
        await app.db.fetchval(
            "SELECT id FROM users WHERE email = $1 OR username = $2", email, username
        )
    ) is not None:
        return True

    return False


async def check_credentials(username, password):
    """Validates the password given by the user against the hashed entry in the database
    Args:
        username: The user's username used for authentication.
        password: The password the user gave us.
    Returns:
        bool: Whether the password is correct or not.
    """
    user = await storage.get_user(username=username, context="auth")
    return bcrypt.checkpw(
        password.encode("utf-8"),
        user["password"].encode("utf-8"),
    )


async def create_user():
    """Creates a user in the database.
    Returns:
        Response: Response object through JSONify.
    """
    req = await request.json
    user = {
        "id": await identifiers.user_id(),
        "username": req.get("username"),
        "email": req.get("email"),
        "password": await hash_password(req.get("password")),
    }

    try:

        assert user["id"] is not None
        assert user["username"] is not None
        assert user["email"] is not None

    except AssertionError:
        raise RuntimeError("User attributes were null during user creation")

    first_user = (await app.db.fetchval("SELECT id FROM users LIMIT 1")) is None

    await app.db.execute(
        "INSERT INTO users (id, username, email, password) VALUES ($1, $2, $3, $4)",
        user.get("id"),
        user.get("username"),
        user.get("email"),
        user.get("password"),
    )

    if first_user:
        await app.db.execute(
            "UPDATE users SET admin = true, verified = true WHERE id = $1",
            user.get("id"),
        )

    if not app.api_config.REGISTRATIONS_REQUIRE_APPROVAL and not first_user:
        await send_mail(user, "verification")
    elif first_user:
        pass
    else:
        await send_mail(user, "signup_admin_approval_required")

    return jsonify(
        {
            "success": True,
            "message": "Your account with userid ({0}) has been created. Please check your email to verify your account.".format(
                user.get("id")
            ),
        }
    )


async def verify_user(user=None):
    return await app.db.execute(
        "UPDATE users SET verified = true WHERE id = $1",
        (request._ctx_user["id"] if user is None else user.get("id")),
    )


async def unverify_user(user=None):
    return await app.db.execute(
        "UPDATE users SET verified = false WHERE id = $1",
        (request._ctx_user["id"] if user is None else user.get("id")),
    )


async def delete_user(user=None):
    await app.db.execute(  # TODO: Actually set up a task to delete anonymized users en-masse along with their uploaded files and metadata.
        "UPDATE users SET username = null, password = null, email = null, verified = false WHERE id = $1",
        (request._ctx_user["id"] if user is None else user.get("id")),
    )


async def delete_unverified_user(user=None):
    return await app.db.execute(
        "DELETE FROM users WHERE id = $1 WHERE verified = false",
        (request._ctx_user["id"] if user is None else user.get("id")),
    )


async def update_email(new_email, user=None):
    _ctx_user = request._ctx_user if user is None else user
    await unverify_user(_ctx_user)

    await app.db.execute(
        "UPDATE users SET email = $1 WHERE id = $2", new_email, _ctx_user["id"]
    )

    _ctx_user["email"] = new_email

    await send_mail(_ctx_user, "verification")


async def update_password(new_password, user=None):
    new_password = await hash_password(new_password)
    return await app.db.execute(
        "UPDATE users SET password = $1 WHERE id = $2",
        new_password,
        (request._ctx_user["id"] if user is None else user.get("id")),
    )


async def hash_password(password):
    return bcrypt.hashpw(password.encode("utf-8"), bcrypt.gensalt()).decode("utf-8")


async def init_mfa(user=None):
    _ctx_user_auth = await storage.get_user(
        user_id=request._ctx_user["id"] if user is None else user.get("id"),
        context="auth",
    )

    mfa_secret = pyotp.random_base32()
    mfa_uri = pyotp.totp.TOTP(mfa_secret).provisioning_uri(
        name=f"{_ctx_user_auth['username']} on {app.api_config.SERVICE_NAME}",
        issuer_name=app.api_config.SERVICE_NAME,
    )

    await app.db.execute(
        "UPDATE users SET mfa_secret = $1 WHERE id = $2",
        mfa_secret,
        _ctx_user_auth["id"],
    )

    return {"mfa_secret": mfa_secret, "mfa_uri": mfa_uri}


async def activate_mfa(code, user=None):
    _ctx_user_auth = await storage.get_user(
        user_id=request._ctx_user["id"] if user is None else user.get("id"),
        context="auth",
    )

    validator = pyotp.TOTP(_ctx_user_auth["mfa_secret"])

    valid = validator.verify(code)

    if not valid:
        raise FailedLogin("Invalid MFA Code.")

    await app.db.execute(
        "UPDATE users SET mfa = true WHERE id = $1", _ctx_user_auth["id"]
    )


async def disable_mfa(user=None):
    await app.db.execute(
        "UPDATE users SET mfa = false, mfa_secret = null WHERE id = $1",
        (request._ctx_user["id"] if user is None else user.get("id")),
    )


async def get_mfa():
    """Fetches the multi-factor authentication code from the request."""
    req = await request.json
    return req["mfa_code"]


async def check_mfa():
    if not request._ctx_user["mfa_enabled"]:
        return True

    try:
        mfa_code = await get_mfa()
        assert mfa_code
    except (TypeError, KeyError, AssertionError):
        raise FailedLogin("Missing MFA code.")

    # TODO: If U2F (future implementation), check what mfa_type the user is using, and use that instead of defaulting to TOTP.
    valid = await _verify_otp(mfa_code)

    if not valid:
        raise FailedLogin("Invalid MFA code.")

    return True


async def _verify_otp(code, user=None):
    _ctx_user_auth = await storage.get_user(
        user_id=request._ctx_user["id"] if user is None else user.get("id"),
        context="auth",
    )

    otp_validator = pyotp.TOTP(_ctx_user_auth["mfa_secret"])

    return otp_validator.verify(code, valid_window=3)


# Token mechanism // A lot of this was inspired by elixire's codebase, as to implement stateless authentication.
async def create_token(user=None, type="timed"):
    """Creates an authentication token.
    Args:
        user_id (str): The ID of the user.
        type (str): Whether the token should expire or not. (Valid types: timed, nontimed, verification)
    Returns:
        string: The authentication token.
    """
    user = await storage.get_user(
        user_id=(request._ctx_user["id"] if user is None else user["id"]),
        context="auth",
    )

    s = TimestampSigner(
        app.api_config.TOKEN_SIGNING_KEY, salt=user["password"].encode("utf-8")
    )

    if type == "nontimed":
        return s.sign(f"u.{user['id']}").decode("utf-8")
    elif type == "verification":
        return s.sign(f"v.{user['id']}").decode("utf-8")
    elif type == "unverified_deletion_token":
        return s.sign(f"d.{user['id']}").decode("utf-8")
    elif type == "password_reset_token":
        return s.sign(f"p.{user['id']}").decode("utf-8")
    elif type == "mfa_auth":
        return s.sign(f"m.{user['id']}").decode("utf-8")
    return s.sign(f"{user['id']}").decode("utf-8")


async def get_token():
    """Fetches the authentication token from the request."""
    try:
        return request.args["token"]
    except KeyError:
        return request.headers["Authorization"]


async def check_token():
    """Checks that the token given in the request is valid for the current authentication context
    Returns:
        user_id (str): The user's ID.
    """
    try:
        token = await get_token()

        assert token
    except (TypeError, KeyError, AssertionError):
        raise FailedLogin("No token provided.")

    data = token.split(".")

    is_upload_token = data[0] == "u"
    is_verification_token = data[0] == "v"
    is_password_reset_token = data[0] == "p"
    is_mfa_token = data[0] == "m"
    is_unverified_deletion_token = token[:2] == "d."

    if (
        is_upload_token
        or is_verification_token
        or is_unverified_deletion_token
        or is_password_reset_token
        or is_mfa_token
    ):
        # If the token is used as an uploader or verification token, select the right part as the user id.
        user_id = data[1]
    else:
        user_id = data[0]

    if not Validate({"user_id": user_id}, TOKEN_CHECK_SCHEMA):
        raise FailedLogin("Invalid token.")

    user = await storage.get_user(user_id=user_id, context="auth")

    if not user:
        raise FailedLogin("Invalid token.")

    if is_mfa_token and request.path != "/login/mfa":
        raise FailedLogin(
            "Multi-factor authentication tokens can only be used with the /login/mfa endpoint."
        )

    if not is_mfa_token and request.path == "/login/mfa":
        raise BadRequest("You are already authenticated.")

    if not is_unverified_deletion_token and request.path == "/user/verify/delete":
        raise FailedLogin(
            "This endpoint requires an Unverified Account Deletion Token."
        )

    if user["verified"] and is_verification_token:
        raise FailedLogin("Verification tokens cannot be used more than once.")

    if user["verified"] and is_unverified_deletion_token:
        raise FailedLogin(
            "Unverified account deletion tokens can only be used while your account is pending approval."
        )

    if is_unverified_deletion_token and request.path != "/user/verify/delete":
        raise FailedLogin(
            "Unverified account deletion tokens cannot be used for authentication, but nice try."
        )

    if (
        (not user["verified"] and not is_verification_token)
        or (is_verification_token and request.path != "/user/verify")
    ) and not is_unverified_deletion_token:
        raise FailedLogin("User is not verified.")

    if is_password_reset_token and request.path != "/reset_password":
        raise FailedLogin(
            "Password reset tokens can only be used with the reset password endpoint."
        )

    if user["banned"]:
        raise FailedLogin("User is banned.")

    salt = user["password"]

    signer = TimestampSigner(app.api_config.TOKEN_SIGNING_KEY, salt=salt)

    token_age = (
        None
        if is_upload_token
        else app.api_config.VERIFICATION_TOKEN_AGE
        if is_verification_token
        else app.api_config.UNVERIFIED_ACCOUNT_DELETION_TOKEN_AGE
        if is_unverified_deletion_token
        else app.api_config.MFA_TOKEN_AGE
        if is_mfa_token
        else app.api_config.TIMED_TOKEN_AGE
    )

    try:
        if token_age is not None:
            signer.unsign(token, max_age=token_age)
        else:
            signer.unsign(token)
    except:
        raise FailedLogin("Invalid token.")

    request._ctx_user = await storage.get_user(user_id=user_id)
