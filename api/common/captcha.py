import aiohttp
from quart import current_app as app


async def verify_captcha(response):
    """Takes the response from the CAPTCHA provider and verifies it with their server
        Args:
            response (str): The response string from the CAPTCHA provider.
        Returns:
            captcha_response["success"] (bool): Whether or not the CAPTCHA response received is valid.
    """
    async with app.aiohttp_session.post(
        app.api_config.CAPTCHA_SITEVERIFY_URL,
        data={"secret": app.api_config.CAPTCHA_SECRET, "response": response},
    ) as captcha_response:
        captcha_response = await captcha_response.json()

        return captcha_response["success"]
