import random

from quart import current_app as app
from uuid import uuid4

from ..errors import APIError


async def user_id():
    """Creates a *unique* user ID in UUIDv4 form.

    Returns:
        str: The *unique* UUIDv4.
    """
    unique = False
    while unique is False:
        user_id = uuid4()

        if (
            await app.db.fetchval("SELECT id FROM users WHERE id = $1", user_id)
        ) is None:
            unique = True

    return user_id


async def file_id():
    """Creates a *unique* file ID in UUIDv4 form.

    Returns:
        str: The *unique* UUIDv4.
    """
    unique = False
    while unique is False:
        file_id = uuid4()

        if (
            await app.db.fetchval("SELECT id FROM files WHERE id = $1", file_id)
        ) is None:
            unique = True

    return file_id


async def file_name(mode="v4", size=None):
    modes = {
        "legacy": {
            "charset": "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789",
            "size": 14,
        },
        "v4": {
            "charset": "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_",
            "size": 16,
        },
    }

    if mode not in modes:
        raise APIError("Invalid mode selected")

    charset = modes.get(mode).get("charset")
    if size is None:
        size = modes.get(mode).get("size")

    unique = False
    while unique is False:
        file_name = "".join(random.choices(charset, k=size))

        if (
            await app.db.fetchval(
                "SELECT file_name FROM files WHERE file_name = $1", file_name
            )
        ) is None:
            unique = True

    return file_name
