from quart import current_app as app
from quart.utils import run_sync

from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail

import smtplib
import email.utils
from email.mime.text import MIMEText

from ..errors import APIError
from ..common import auth  # Hack to bypass circular importing

from ..templates.email import (
    VERIFICATION_EMAIL,
    SIGNUP_ADMIN_APPROVAL_REQUIRED_EMAIL,
    PASSWORD_RESET_EMAIL,
)


async def send_mail(user, email_type):
    """Sends out an email to the given user based on the given email type.
        Args:
            user (obj): A user object.
            email_type (str): The type of email to send (verification, signup_admin_approval_required, password_reset)
    """

    email_provider = app.api_config.EMAIL_PROVIDER.lower()

    if email_type == "verification":
        subject = f"Verify your account on {app.api_config.SERVICE_NAME}"
        email_content = VERIFICATION_EMAIL.format(
            username=user["username"],
            token=await auth.create_token(user, type="verification"),
            service_name=app.api_config.SERVICE_NAME,
            base_url=app.api_config.BASE_URL,
        )

    elif email_type == "signup_admin_approval_required":
        subject = f"Your account on {app.api_config.SERVICE_NAME} | Next steps"
        email_content = SIGNUP_ADMIN_APPROVAL_REQUIRED_EMAIL.format(
            username=user["username"],
            token=await auth.create_token(user, type="unverified_deletion_token"),
            support_email=app.api_config.SUPPORT_EMAIL,
            service_name=app.api_config.SERVICE_NAME,
            base_url=app.api_config.BASE_URL,
        )

    elif email_type == "password_reset":
        subject = f"Reset your password | {app.api_config.SERVICE_NAME}"
        email_content = PASSWORD_RESET_EMAIL.format(
            username=user["username"],
            token=await auth.create_token(user, type="password_reset_token"),
            service_name=app.api_config.SERVICE_NAME,
            base_url=app.api_config.BASE_URL,
        )

    else:
        raise APIError(f"No such email type defined: {email_type}")

    if email_provider == "sendgrid":
        await run_sync(_send_email_sendgrid)(
            mail_to=user["email"], subject=subject, msg=email_content
        )
    elif email_provider == "smtp":
        await run_sync(_send_email_smtp)(
            mail_to=user["email"], subject=subject, msg=email_content
        )

    else:
        raise APIError(f"No such email provider defined: {email_provider}")


def _send_email_sendgrid(mail_to, subject, msg):
    """This is an internal function used to send email through the Sendgrid API"""
    message = Mail(
        from_email=app.api_config.SEND_FROM_EMAIL,
        to_emails=mail_to,
        subject=subject,
        plain_text_content=msg,
    )
    sg = SendGridAPIClient(app.api_config.SENDGRID_API_KEY)
    sg.send(message)


def _send_email_smtp(mail_to, subject, msg):
    """This is an internal function used to send email through SMTP"""
    msg = MIMEText(msg)
    msg.set_unixfrom(app.api_config.SERVICE_NAME)

    msg["To"] = email.utils.formataddr(("Recipient", mail_to))
    msg["From"] = email.utils.formataddr(("Author", app.api_config.SEND_FROM_EMAIL))
    msg["Subject"] = subject

    server = smtplib.SMTP(host=app.api_config.SMTP_HOST, port=app.api_config.SMTP_PORT)
    try:
        server.ehlo()

        if server.has_extn("STARTTLS"):
            server.starttls()
            server.ehlo()

        server.login(
            user=app.api_config.SMTP_USERNAME, password=app.api_config.SMTP_PASSWORD
        )
        server.sendmail(
            from_addr=app.api_config.SEND_FROM_EMAIL,
            to_addrs=[mail_to],
            msg=msg.as_string(),
        )
    finally:
        server.quit()
