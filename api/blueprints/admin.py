from quart import Blueprint, Response
from quart import json
from quart.wrappers import response
from quart import current_app as app
from quart import request, jsonify

from .. import storage

from ..errors import BadRequest, FailedLogin
from ..schema import Validate, ADMIN_ACTIVATE_USER_SCHEMA

from ..common.auth import check_mfa, check_token, verify_user

bp = Blueprint("admin", __name__)


@bp.route("/admin/users", methods=["GET"])
async def _admin_get_users():
    await check_token()
    await check_mfa()

    if not request._ctx_user["admin"]:
        raise FailedLogin("Not an administrator.")

    status_type = request.args.get("status", None)
    if status_type is not None:
        status_type = status_type.lower()

        if status_type == "active":
            users = await storage.get_users(constraint="active")
        elif status_type == "inactive":
            users = await storage.get_users(constraint="inactive")
        elif status_type == "banned":
            users = await storage.get_users(constraint="banned")
        else:
            users = await storage.get_users()
    else:
        users = await storage.get_users()

    return jsonify(users)


@bp.route("/admin/users/activate", methods=["POST"])
async def _admin_activate_users():
    await check_token()
    await check_mfa()

    if not request._ctx_user["admin"]:
        raise FailedLogin("Not an administrator.")

    req = await request.json

    if not Validate(req, ADMIN_ACTIVATE_USER_SCHEMA):
        raise BadRequest("Invalid data.")

    user_to_activate = await storage.get_user(user_id=req["user_id"])

    if not user_to_activate["verified"]:
        await verify_user(user_to_activate)
    else:
        raise BadRequest("User is already active.")

    return jsonify({"success": True})
