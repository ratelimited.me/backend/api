import config
from minio import Minio

from quart import Blueprint, Response
from quart.wrappers import response
from quart import current_app as app
from quart import request, jsonify
from quart.utils import run_sync

from .. import storage

from ..common import identifiers
from ..common.auth import check_token
from ..common.uploads import write_file_to_db, check_bucket

from ..errors import BadRequest, FailedUpload

bp = Blueprint("upload", __name__)


@bp.route("/upload", methods=["GET"])
async def upload_methods_list():
    return jsonify(
        {
            "available": {
                "/upload": "soonTM",
                "/upload/pomf": "POMF-spec compatible upload endpoint. ?key, ?token, or the Authorization header are supported as authentication methods.",
            }
        }
    )


@bp.route("/upload/pomf", methods=["POST"])
async def upload_pomf():
    await check_token()

    file = (await request.files).get("files[]")

    if file is None:
        raise BadRequest("File is missing, are you sure you're using files[]?")

    length = len(await run_sync(file.stream.getvalue)())

    maximum_file_size = request._ctx_user.get(
        "maximum_upload_size", app.api_config.DEFAULT_UPLOAD_LIMIT * 1024 * 1024
    )

    if not length <= maximum_file_size:
        raise FailedUpload(
            f"File with size {0} exceeds maximum file size of {1}".format(
                length, maximum_file_size
            )
        )

    file_extension = (
        ".no_extension_when_uploading"
        if file.filename.count(".") == 0
        else "." + file.filename.split(".")[-1]
    )
    file_name = await identifiers.file_name() + file_extension

    bucket = await check_bucket(request)

    await write_file_to_db(
        request._ctx_user, await identifiers.file_id(), file_name, file.filename, length
    )

    await run_sync(app.minio.put_object)(
        bucket_name=bucket,
        object_name=file_name,
        data=file,
        length=length,
        content_type=file.content_type,
    )

    return jsonify(
        {
            "file": {
                "name": file.filename,
                "length": length,
                "type": file.content_type,
            },
            "url": file_name,
        }
    )
