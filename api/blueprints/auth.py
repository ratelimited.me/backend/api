from quart import Blueprint, Response
from quart import json
from quart.wrappers import response
from quart import current_app as app
from quart import request, jsonify

from datetime import timedelta
from quart_rate_limiter import RateLimit, rate_limit, rate_exempt

import pyotp

from ..schema import Validate, LOGIN_SCHEMA, LOGIN_MFA_TOTP_SCHEMA, REGISTRATION_SCHEMA
from ..errors import BadRequest, FailedLogin, FeatureDisabled
from ..common import identifiers
from ..common.captcha import verify_captcha
from ..common.auth import (
    check_credentials,
    check_mfa,
    check_token,
    create_token,
    user_exists,
    create_user,
)

from .. import storage

bp = Blueprint("auth", __name__)


@bp.route("/register", methods=["POST"])
@bp.route("/signup", methods=["POST"])
@rate_limit(1, timedelta(minutes=20))
async def register():
    """This is the route called when /register or /signup is hit with a POST request.

    Raises:
        FeatureDisabled: This returns a HTTP 503 error, raised when registrations are disabled.
        BadRequest: Returns a HTTP 400 error, raised either when the request fails to validate, or when the user already exists.

    Returns:
        Response: A JSONify response object [see: create_user()]
    """
    if not app.api_config.REGISTRATIONS_ENABLED:
        raise FeatureDisabled("Registrations are currently disabled")

    req = await request.json

    if not Validate(req, REGISTRATION_SCHEMA):
        raise BadRequest("Bad request contents")

    if app.api_config.REGISTRATIONS_REQUIRE_CAPTCHA:
        if not req.get("captcha_response"):
            raise BadRequest("Bad request contents")

        if not await verify_captcha(req["captcha_response"]):
            raise BadRequest("Invalid captcha response")

    if await user_exists(req.get("email"), req.get("username")):
        raise BadRequest("A user with this email or username already exists")

    return await create_user()


@bp.route("/login", methods=["POST"])
@rate_limit(
    limits=[RateLimit(1, timedelta(seconds=10)), RateLimit(5, timedelta(minutes=3))]
)
async def login():
    """This is the route called when /login is with with a POST request.

    Raises:
        BadRequest: Returns a HTTP 400 error, raised when the request fails to validate.
        FailedLogin: Returns a HTTP 401 error, raised either when the user does not exist, or an invalid username-password combination is given

    Returns:
        Response: A JSONify response object [see: create_token()]
    """
    req = await request.json

    if not Validate(req, LOGIN_SCHEMA):
        raise BadRequest("Bad request contents")

    if not await user_exists(None, req.get("username")):
        raise FailedLogin("Invalid credentials")

    if not await check_credentials(req.get("username"), req.get("password")):
        raise FailedLogin("Invalid credentials")

    request._ctx_user = await storage.get_user(username=req.get("username"))

    return jsonify(
        {
            "token": (
                await create_token()
                if not request._ctx_user["mfa_enabled"]
                else await create_token(type="mfa_auth")
            )
        }
    )


@bp.route("/login/mfa", methods=["POST"])
@rate_limit(
    limits=[RateLimit(1, timedelta(seconds=5)), RateLimit(5, timedelta(minutes=1))]
)
async def mfa_check_route():
    req = await request.json

    if not Validate(req, LOGIN_MFA_TOTP_SCHEMA):
        raise BadRequest("Bad request contents")

    await check_token()

    if not request._ctx_user["mfa_enabled"]:
        raise FailedLogin("User does not have MFA enabled.")

    await check_mfa()

    return jsonify({"token": await create_token()})
