from quart import Blueprint, Response
from quart import json
from quart.wrappers import response
from quart import current_app as app
from quart import request, jsonify

from quart_rate_limiter import rate_limit
from datetime import timedelta

from .. import storage
from ..schema import (
    Validate,
    RESET_PASSWORD_SCHEMA_ACTION,
    RESET_PASSWORD_SCHEMA_GENERIC,
    RESET_PASSWORD_SCHEMA_REQUEST,
    UPDATE_USER_SCHEMA,
    UPDATE_USER_SCHEMA_UPDATE_FIELD,
    UPDATE_USER_SCHEMA_UPDATE_FIELD_EMAIL,
    UPDATE_USER_SCHEMA_UPDATE_FIELD_PASSWORD,
    UPDATE_USER_SCHEMA_ACTIVATE_MFA,
    UPDATE_USER_SCHEMA_DISABLE_MFA,
    UPDATE_USER_SCHEMA_INIT_MFA,
    CREATE_UPLOAD_TOKEN_SCHEMA,
    DELETE_UPLOAD_TOKEN_SCHEMA,
)
from ..errors import BadRequest, FailedLogin
from ..common.auth import (
    activate_mfa,
    check_credentials,
    check_mfa,
    check_token,
    create_token,
    delete_unverified_user,
    delete_user,
    disable_mfa,
    init_mfa,
    update_email,
    update_password,
    verify_user,
)
from ..common.email import send_mail

bp = Blueprint("user", __name__)


@bp.route("/user", methods=["GET"])
async def get_user_route():
    """Fetches information about the currently-authenticated user."""
    await check_token()
    return jsonify(request._ctx_user)


@bp.route("/user", methods=["DELETE"])
async def delete_user_route():
    await check_token()
    await check_mfa()

    await delete_user()

    return jsonify({"success": True})


@bp.route("/user/verify", methods=["GET"])
async def verify_user_route():
    """When paired with a token query parameter, verifies the email of an account."""
    await check_token()
    await verify_user()

    return jsonify({"success": True})


@bp.route("/user/verify/delete", methods=["GET", "DELETE"])
async def delete_unverified_user_route():
    await check_token()
    await delete_unverified_user()

    return jsonify({"success": True})


@bp.route("/reset_password", methods=["POST"])
@rate_limit(4, timedelta(minutes=10))
async def _reset_user_password_route():
    req = await request.json
    if not Validate(req, RESET_PASSWORD_SCHEMA_GENERIC):
        raise BadRequest("Invalid data.")

    action = req["action"].lower()
    if action == "request":
        if not Validate(req, RESET_PASSWORD_SCHEMA_REQUEST):
            raise BadRequest("Invalid data.")

        user = await storage.get_user(email=req["email"])
        await send_mail(user, "password_reset")

        return jsonify({"success": True})
    elif action == "reset":
        if not Validate(req, RESET_PASSWORD_SCHEMA_ACTION):
            raise BadRequest("Invalid data.")

        await check_token()
        await update_password(req["password"])

        return jsonify({"success": True})


@bp.route("/user", methods=["POST"])
async def modify_user_route():
    """Allows a user to modify their data."""
    req = await request.json
    if not Validate(req, UPDATE_USER_SCHEMA):
        raise BadRequest("Invalid data.")

    await check_token()

    action = req["action"].lower()

    if action == "update_field":
        if not Validate(req, UPDATE_USER_SCHEMA_UPDATE_FIELD):
            raise BadRequest("Invalid data.")

        if req["field"] == "email":
            if not Validate(req, UPDATE_USER_SCHEMA_UPDATE_FIELD_EMAIL):
                raise BadRequest("Invalid data.")

            if not await check_credentials(
                request._ctx_user["username"], req["password"]
            ):
                raise FailedLogin("Invalid credentials.")

            if req["value"] == request._ctx_user["email"]:
                raise BadRequest("You can't update your account with the same email.")

            await check_mfa()
            await update_email(req["value"])

            return jsonify({"success": True})
        elif req["field"] == "password":
            if not Validate(req, UPDATE_USER_SCHEMA_UPDATE_FIELD_PASSWORD):
                raise BadRequest("Invalid data.")

            if not await check_credentials(
                request._ctx_user["username"], req["password"]
            ):
                raise FailedLogin("Invalid credentials.")

            await check_mfa()
            await update_password(req["value"])

            return jsonify({"success": True})

    elif action == "init_mfa":
        if not Validate(req, UPDATE_USER_SCHEMA_INIT_MFA):
            raise BadRequest("Invalid data.")

        if not await check_credentials(request._ctx_user["username"], req["password"]):
            raise FailedLogin("Invalid credentials.")

        if request._ctx_user["mfa_enabled"]:
            raise BadRequest("MFA is already enabled for your account.")

        init = await init_mfa()

        return jsonify(
            {
                "success": True,
                "mfa": {"secret": init["mfa_secret"], "uri": init["mfa_uri"]},
            }
        )
    elif action == "activate_mfa":
        if not Validate(req, UPDATE_USER_SCHEMA_ACTIVATE_MFA):
            raise BadRequest("Invalid data.")

        if not await check_credentials(request._ctx_user["username"], req["password"]):
            raise FailedLogin("Invalid credentials.")

        if request._ctx_user["mfa_enabled"]:
            raise BadRequest("MFA is already enabled for your account.")

        await activate_mfa(req["mfa_code"])

        return jsonify({"success": True})
    elif action == "disable_mfa":
        if not Validate(req, UPDATE_USER_SCHEMA_DISABLE_MFA):
            raise BadRequest("Invalid data.")

        if not await check_credentials(request._ctx_user["username"], req["password"]):
            raise FailedLogin("Invalid credentials.")

        if not request._ctx_user["mfa_enabled"]:
            raise BadRequest("MFA is not enabled for your account.")

        await check_mfa()
        await disable_mfa()

        return jsonify({"success": True})


@bp.route("/user/tokens", methods=["PUT", "POST"])
@rate_limit(1, timedelta(seconds=10))
async def create_user_tokens_route():
    """Creates a non-timed ("upload") authentication token."""
    req = await request.json

    if not Validate(req, CREATE_UPLOAD_TOKEN_SCHEMA):
        raise BadRequest("Invalid data.")

    await check_token()
    await check_mfa()

    if not await check_credentials(request._ctx_user["username"], req["password"]):
        raise FailedLogin("Invalid credentials.")

    return jsonify({"token": await create_token(type="nontimed")})


@bp.route("/user/tokens", methods=["DELETE"])
async def delete_user_tokens_route():
    """Revokes all timed & non-timed authentication tokens."""
    req = await request.json

    if not Validate(req, DELETE_UPLOAD_TOKEN_SCHEMA):
        raise BadRequest("Invalid data.")

    await check_token()
    await check_mfa()

    if not await check_credentials(request._ctx_user["username"], req["password"]):
        raise FailedLogin("Invalid credentials.")

    await update_password(req.get("password"))

    return jsonify({"success": True})